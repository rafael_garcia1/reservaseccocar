### Prueba API Reserva EccoCar

### Description:
This project implements a REST API for booking management. The API allows you to create new bookings and get a total count of existing bookings.

### Author
#### Ing. Rafael García Acosta.
3046473107 - ingrafaelgarciaacosta@gmail.com

### Technologies:

##### Programming language:
- Java 17
- Spring Boot
- Spring Web
- Spring Data JPA
- H2 Database
- IntelliJ IDEA
- Maven

### Test link:

This is the [Link for the documentation](http://localhost:8080/swagger-ui/index.html#/)








