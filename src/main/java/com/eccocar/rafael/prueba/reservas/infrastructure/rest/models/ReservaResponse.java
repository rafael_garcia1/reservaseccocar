package com.eccocar.rafael.prueba.reservas.infrastructure.rest.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * Model representing a reservation response sent through the REST API.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReservaResponse {
    /**
     * This is the unique identifier for the reservation
     */
    private Long id;
    /**
     * This is the license plate of the vehicle
     */
    private String licensePlate;
    /**
     * This is the name of the person who made the reservation
     */
    private String name;
    /**
     * This is the contact number of the person who made the reservation
     */
    private String contactNumber;
    /**
     * This is the date and time when the reservation starts
     */
    private LocalDateTime start;

}
