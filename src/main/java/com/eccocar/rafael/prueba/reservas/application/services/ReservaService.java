package com.eccocar.rafael.prueba.reservas.application.services;

import com.eccocar.rafael.prueba.reservas.application.repositories.ReservaRepository;
import com.eccocar.rafael.prueba.reservas.domain.Reserva;
import com.eccocar.rafael.prueba.reservas.infrastructure.db.converters.ReservaEntityConverter;
import com.eccocar.rafael.prueba.reservas.infrastructure.db.entities.ReservaEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * Service class responsible for managing Reserva operations.
 */
@Service
public class ReservaService {
    @Autowired
    private ReservaRepository repository;

    /**
     * Creates a new Reserva
     */
    public Reserva createReserva(Reserva reserva) {
        reserva.setStartReserva(LocalDateTime.now());
        ReservaEntity entity = repository.save(ReservaEntityConverter.from(reserva));
        return ReservaEntityConverter.to(entity);
    }

    /**
     * Counts the total number of reservas.
     */

    public Map<String, Long> countTotal() {
        Map<String, Long> total = new HashMap();
        total.put("total", repository.count());
        return total;
    }
}
