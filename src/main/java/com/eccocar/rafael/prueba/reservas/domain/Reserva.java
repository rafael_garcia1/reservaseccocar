package com.eccocar.rafael.prueba.reservas.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * This domain class handles the data for reservas
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Reserva {
    /**
     * This is the unique identifier for the reservation
     */
    private Long idReserva;
    /**
     * This is the license plate of the vehicle
     */
    private String licensePlate;
    /**
     * This is the name of the person who made the reservation
     */
    private String name;
    /**
     * This is the contact number of the person who made the reservation
     */
    private String contactNumber;
    /**
     * This is the date and time when the reservation starts
     */
    private LocalDateTime startReserva;
    /**
     * This is the date and time when the reservation ends
     */
    private LocalDateTime endReserva;

    public Reserva(String licensePlate, String name, String contactNumber) {
        this.licensePlate = licensePlate;
        this.name = name;
        this.contactNumber = contactNumber;
    }
}
