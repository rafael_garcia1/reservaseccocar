package com.eccocar.rafael.prueba.reservas.infrastructure.db.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * This class hanldes the information in DB of reservas
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ReservaEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * This is the unique identifier for the reservation
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ReservaEntityGenerator")
    private Long idReserva;
    /**
     * This is the license plate of the vehicle
     */
    private String licensePlate;
    /**
     * This is the name of the person who made the reservation
     */
    private String name;
    /**
     * This is the contact number of the person who made the reservation
     */
    private String contactNumber;
    /**
     * This is the date and time when the reservation starts
     */
    private LocalDateTime startReserva;
    /**
     * This is the date and time when the reservation ends
     */
    private LocalDateTime endReserva;
}
