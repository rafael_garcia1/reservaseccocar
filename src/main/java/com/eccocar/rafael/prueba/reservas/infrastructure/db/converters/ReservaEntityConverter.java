package com.eccocar.rafael.prueba.reservas.infrastructure.db.converters;

import com.eccocar.rafael.prueba.reservas.domain.Reserva;
import com.eccocar.rafael.prueba.reservas.infrastructure.db.entities.ReservaEntity;

/**
 * Utility class responsible for converting between Reserva and ReservaEntity.
 */
public class ReservaEntityConverter {

    /**
     * Prevents instantiation of this utility class.
     */
    private ReservaEntityConverter() {
        throw new IllegalStateException("This is an utility class");
    }

    /**
     * Converts Reserva to ReservaEntity.
     */
    public static ReservaEntity from(Reserva reserva) {
        return new ReservaEntity(reserva.getIdReserva(), reserva.getLicensePlate(),
                reserva.getName(), reserva.getContactNumber(), reserva.getStartReserva(),
                reserva.getEndReserva());
    }

    /**
     * Converts ReservaEntity to Reserva.
     */
    public static Reserva to(ReservaEntity reserva) {
        return new Reserva(reserva.getIdReserva(), reserva.getLicensePlate(),
                reserva.getName(), reserva.getContactNumber(), reserva.getStartReserva(),
                reserva.getEndReserva());
    }

}
