package com.eccocar.rafael.prueba.reservas.infrastructure.rest.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Model representing a reservation request sent through the REST API.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReservaRequest {
    /**
     * This is the license plate of the vehicle
     */
    private String licensePlate;
    /**
     * This is the name of the person who made the reservation
     */
    private String name;
    /**
     * This is the contact number of the person who made the reservation
     */
    private String contactNumber;
}
