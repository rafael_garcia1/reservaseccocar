package com.eccocar.rafael.prueba.reservas.application.repositories;

import com.eccocar.rafael.prueba.reservas.infrastructure.db.entities.ReservaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository interface for managing ReservaEntity instances.
 */
@Repository
public interface ReservaRepository extends JpaRepository<ReservaEntity, Long> {
}
