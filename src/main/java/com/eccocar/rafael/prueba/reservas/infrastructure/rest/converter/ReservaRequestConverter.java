package com.eccocar.rafael.prueba.reservas.infrastructure.rest.converter;

import com.eccocar.rafael.prueba.reservas.domain.Reserva;
import com.eccocar.rafael.prueba.reservas.infrastructure.rest.models.ReservaRequest;

/**
 * Utility class responsible for converting ReservaRequest to Reserva.
 */
public class ReservaRequestConverter {

    /**
     * Prevents instantiation of this utility class.
     */
    private ReservaRequestConverter() {
        throw new IllegalStateException("This is an utility class");
    }

    /**
     * Converts a ReservaRequest to Reserva.
     */
    public static Reserva to(ReservaRequest reserva) {
        return new Reserva(reserva.getLicensePlate(), reserva.getName(), reserva.getContactNumber());
    }
}
