package com.eccocar.rafael.prueba.reservas.infrastructure.rest.api;

import com.eccocar.rafael.prueba.reservas.application.services.ReservaService;
import com.eccocar.rafael.prueba.reservas.domain.Reserva;
import com.eccocar.rafael.prueba.reservas.infrastructure.rest.converter.ReservaRequestConverter;
import com.eccocar.rafael.prueba.reservas.infrastructure.rest.converter.ReservaResponseConverter;
import com.eccocar.rafael.prueba.reservas.infrastructure.rest.models.ReservaRequest;
import com.eccocar.rafael.prueba.reservas.infrastructure.rest.models.ReservaResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * REST controller for managing Reserva operations.
 */
@Controller
@RestController
@RequestMapping("/reserva")
public class ReservaController {

    @Autowired
    ReservaService reservaService;

    /**
     * Creates a new Reserva.
     */
    @PostMapping()
    public ReservaResponse createReserva(ReservaRequest request) {
        Reserva reserva = reservaService.createReserva(ReservaRequestConverter.to(request));
        return ReservaResponseConverter.from(reserva);
    }

    /**
     * Retrieves the total count of Reserva instances.
     */
    @GetMapping("/all")
    public Map<String, Long> totalReservas() {
        return reservaService.countTotal();
    }

}
