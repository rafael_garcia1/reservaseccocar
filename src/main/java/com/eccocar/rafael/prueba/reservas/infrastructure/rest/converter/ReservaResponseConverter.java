package com.eccocar.rafael.prueba.reservas.infrastructure.rest.converter;

import com.eccocar.rafael.prueba.reservas.domain.Reserva;
import com.eccocar.rafael.prueba.reservas.infrastructure.rest.models.ReservaResponse;

/**
 * Utility class for converting Reserva to ReservaResponse.
 */
public class ReservaResponseConverter {

    /**
     * Prevents instantiation of this utility class.
     */
    private ReservaResponseConverter() {
        throw new IllegalStateException("This is an utility class");
    }

    /**
     * Converts Reserva to ReservaResponse.
     *
     * @param reserva the {@link Reserva} to convert
     * @return the converted {@link ReservaResponse}
     */
    public static ReservaResponse from(Reserva reserva) {
        return new ReservaResponse(reserva.getIdReserva(), reserva.getLicensePlate(),
                reserva.getName(), reserva.getContactNumber(), reserva.getStartReserva());
    }
}
